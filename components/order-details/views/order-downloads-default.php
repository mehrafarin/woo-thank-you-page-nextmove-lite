<?php

defined( 'ABSPATH' ) || exit;

echo '<br><div class="xlwcty_title">' . __( 'Downloads', 'woocommerce' ) . '</div>';

?>

<table class="woocommerce-table woocommerce-table--order-downloads shop_table shop_table_responsive order_details">
    <thead>
    <tr>
		<?php foreach ( wc_get_account_downloads_columns() as $column_id => $column_name ) : ?>
            <?php if(in_array($column_id, ['download-product', 'download-file'])): ?>
            <th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
            <?php endif; ?>
		<?php endforeach; ?>
    </tr>
    </thead>

	<?php foreach ( $downloads as $download ) : ?>
        <tr>
			<?php foreach ( wc_get_account_downloads_columns() as $column_id => $column_name ) : ?>
          <?php if(in_array($column_id, ['download-product', 'download-file'])): ?>
                <td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
          <?php endif; ?>
					<?php
					if ( has_action( 'woocommerce_account_downloads_column_' . $column_id ) ) {
						do_action( 'woocommerce_account_downloads_column_' . $column_id, $download );
					} else {
						switch ( $column_id ) {
							case 'download-product':
								if ( $download['product_url'] ) {
									echo '<a href="' . esc_url( $download['product_url'] ) . '">' . esc_html( $download['product_name'] ) . '</a>';
								} else {
									echo esc_html( $download['product_name'] );
								}
								break;
							case 'download-file':
								echo '<a href="' . esc_url( $download['download_url'] ) . '" class="button">' . esc_html( $download['download_name'] ) . '</a>';
								break;
						}
					}
					?>
                </td>
			<?php endforeach; ?>
        </tr>
	<?php endforeach; ?>
</table>
